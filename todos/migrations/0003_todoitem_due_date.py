# Generated by Django 4.1.1 on 2022-09-07 19:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("todos", "0002_todoitem"),
    ]

    operations = [
        migrations.AddField(
            model_name="todoitem",
            name="due_date",
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
